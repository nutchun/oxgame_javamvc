package com.tictactoe; 

import java.util.Scanner;

public class Display {

	public static void main(String[] args) {
		Board board = new Board();
		Controller ctrl = new Controller(board);
		Display display = new Display();
		
		System.out.print("Board size: ");
		@SuppressWarnings("resource")
		Scanner tsize = new Scanner(System.in);
		board.setTableSize(tsize.nextInt());

		int size = board.getTableSize();
		int winner = 0;

		@SuppressWarnings("resource")
		Scanner usr = new Scanner(System.in);

		while (winner == 0) {
			winner = board.getWinner();
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < size; j++) {
					if (j == 0) {
						System.out.print("                   ");
					} else if (j < size - 1) {
						System.out.print("#                   ");
					} else {
						System.out.println("#                   ");
					}
				}
			}
			for (int n = 0; n < size; n++) {
				for (int i = 0; i < 7; i++) {
					for (int j = 0; j < size; j++) {
						int val = board.getValue(n, j);
						display.drawOX_mark(val, i);
						if (j < size - 1) {
							System.out.print("#");
						} else {
							System.out.println("");
						}
					}
				}
				if (n < size - 1) {
					for (int i = 0; i < 2; i++) {
						for (int j = 0; j < size; j++) {
							if (j == 0) {
								System.out.print("                   ");
							} else if (j < size - 1) {
								System.out.print("#                   ");
							} else {
								System.out.println("#                   ");
							}
						}
					}
					for (int j = 0; j < size; j++) {
						if (j == 0) {
							System.out.print("###################");
						} else if (j < size - 1) {
							System.out.print("####################");
						} else {
							System.out.println("####################");
						}
					}
					for (int i = 0; i < 2; i++) {
						for (int j = 0; j < size; j++) {
							if (j == 0) {
								System.out.print("                   ");
							} else if (j < size - 1) {
								System.out.print("#                   ");
							} else {
								System.out.println("#                   ");
							}
						}
					}
				}
			}
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < size; j++) {
					if (j == 0) {
						System.out.print("                   ");
					} else if (j < size - 1) {
						System.out.print("#                   ");
					} else {
						System.out.println("#                   ");
					}
				}
			}
			if (winner == 0) {
				int pos = usr.nextInt();
				ctrl.selectCell(pos);
			}
		}
		if (winner == 1) {
			System.out.println("O is winner!");
		} else if (winner == 2) {
			System.out.println("X is winner!");
		} else if (winner == 3) {
			System.out.println("Draw!");
		}
	}

	public void drawOX_mark(int select, int lnnum) {
		System.out.print("     ");
		if (select == 0) {
			System.out.print("         ");
		} else if (select == 1) {
			switch (lnnum) {
			case 0:
				System.out.print("  _____  ");
				break;
			case 1:
				System.out.print(" / ___ \\ ");
				break;
			case 2:
				System.out.print("| |   | |");
				break;
			case 3:
				System.out.print("| |   | |");
				break;
			case 4:
				System.out.print("| |___| |");
				break;
			case 5:
				System.out.print(" \\_____/ ");
				break;
			case 6:
				System.out.print("         ");
				break;
			}
		} else if (select == 2) {
			switch (lnnum) {
			case 0:
				System.out.print(" _    _  ");
				break;
			case 1:
				System.out.print("\\ \\  / / ");
				break;
			case 2:
				System.out.print(" \\ \\/ /  ");
				break;
			case 3:
				System.out.print("  )  (   ");
				break;
			case 4:
				System.out.print(" / /\\ \\  ");
				break;
			case 5:
				System.out.print("/_/  \\_\\ ");
				break;
			case 6:
				System.out.print("         ");
				break;
			}
		}
		System.out.print("     ");
	}
}
