package com.tictactoe;

public class Controller {
	private Board board;

	public Controller(Board table) {
		this.board = table;
	}

	public void selectCell(int cellnum) {
		int size = board.getTableSize();
		if (cellnum > 0 && cellnum < size * size + 1) {
			for (int n = 0; n < size; n++) {
				if (cellnum > size * n && cellnum <= size * (n + 1)) {
					int x = n, y;
					if (cellnum % size == 0) {
						y = size - 1;
					} else {
						y = cellnum % size - 1;
					}
					if (board.getValue(x, y) == 0) {
						board.setValue(x, y, board.getTurn() + 1);
						board.setTurn(1 - board.getTurn());
						board.setFull();
					}
				}
			}
		}
	}
}
